import pandas as pd
import os
from openpyxl import load_workbook
from datetime import datetime
from time import sleep

def process_data(abs_file_path):
    df = pd.read_excel(abs_file_path, engine = 'openpyxl')
    df = df.fillna('null')
    date_list = df['入帳日期'].to_list()
    date_list_unique = sorted(pd.unique(date_list))  # 日期list處理, 去除重複
    # print(date_list_unique)

    # ======把每一個日期做成迭代, 用next()一個一個處理完就丟出來=======
    df_select = (df[df['入帳日期'] == i] for i in date_list_unique)
    # print(next(df_select))

    area_BinChg = '濱江'
    area_ShiLin = '士林'
    area_Noarea = '全廠'

    data_BinChg = sepreate_data(abs_file_path , area_BinChg)
    select_insrt_save_report(date_list_unique, area_BinChg, data_BinChg)

    # sleep(2)

    data_ShiLin = sepreate_data(abs_file_path , area_ShiLin)
    select_insrt_save_report(date_list_unique, area_ShiLin, data_ShiLin)

    data_Noarea = sepreate_data(abs_file_path , area_Noarea)
    select_insrt_save_report(date_list_unique, area_Noarea, data_Noarea)

    save_report_path = select_report().replace('templates', 'output')

    print('轉出完畢 : 打開輸出資料夾')
    os.system(f'start {save_report_path}')
    
def select_insrt_save_report(date_list_unique, report_area, results):
    # ========建立報表路徑=======
    report_date_1 = date_list_unique[0].replace('-', '')
    print('報表日期 : ' + report_date_1)
    report_data_name = date_list_unique[0].replace('-', '')
    which_report_name = f'{report_data_name[2:6]}{report_area}營收日報表.xlsx'
    print('呼叫報表 : ' + which_report_name)
    # print(results)
    call_report_path = f'{select_report()}/{which_report_name}'
    # print('報表路徑 : ' + call_report_path)
    # ========建立報表路徑=======


    wb = load_workbook(call_report_path) # 讀取路徑, 開啟EXCEL
    sheet = wb[f'110{report_date_1[4:6]}'] # 打開對應活頁

    column_1st = [i.value for i in list(sheet.columns)[0]]  # 第一column, 抓出日期
    row_name = [i.value for i in list(sheet.rows)[4]] # 取出橫軸column name


    column_name = ['刷卡（含稅）', '現金/電匯\n（含稅）', '應收帳款\n（含稅）', '內轉（含稅）']
    report_data = (i for i in results)
    for i in range(len(results)):
        data_report = next(report_data)

        for j in column_name:
            sheet.cell(
                row = column_1st.index(results[i]['日期']) + 1,
                column = row_name.index(j) + 1,
                value = data_report[j]
            )
    # 存檔到對應位置 output folder
    save_report_path = select_report().replace('templates', 'output')
    # 做檔名存檔時間結尾
    sys_time = datetime.now().strftime('%m%d%I%M%S')
    save_report_name = f'{report_data_name[2:6]}{report_area}營收日報表_{sys_time}.xlsx'
    save_file_path = f'{save_report_path}/{save_report_name}'
    print('轉出路徑 : ' + save_file_path)
    wb.save(save_file_path)
    wb.close()

    # return save_report_path

def sepreate_data(abs_file_path, area_mode):
    df = pd.read_excel(abs_file_path, engine = 'openpyxl')
    df = df.fillna('null')
    date_list = df['入帳日期'].to_list()
    date_list_unique = sorted(pd.unique(date_list))  # 日期list處理, 去除重複
    # print(date_list_unique)

    # ======把每一個日期做成迭代, 用next()一個一個處理完就丟出來=======
    df_select = (df[df['入帳日期'] == i] for i in date_list_unique)
    # print(next(df_select))
    results = []
    for i in range(len(date_list_unique)):  # 報表裡面含多少日期, 設定迴圈次數, 取出每個日期的資料
        df_modify_1 = next(df_select)  # 用next() 一次輸出一個資料做處理# print(type(df_modify)) #確認一下type為Dataframe
    # ======把個別要的資料取出來======
        data_bank_dep_info = df_modify_1['部門'].to_list() # 這邊就讓資料分流?
        # =======做廠別分流====== 用部門的list 做判斷, 取出index位置
        data_index = []
        for ii in range(len(data_bank_dep_info)):
            if data_bank_dep_info[ii][-2:] == area_mode:
                data_index.append(ii)
            elif area_mode == '全廠':
                data_index.append(ii)
        # print(data_index)


        # for kk in data_BinChg_index:
        #     print(kk) optput : 0,1,2

        # 濱江 & 士林 data 分開做, 要組成新的list, 原本的list要過加工
        # print(df_modify[0:1])
        df_modify = df_modify_1.iloc[data_index] # 取BINGCHG的row

        data_bank_work_num = df_modify['工單號碼'].to_list()
        data_bank_date = df_modify['入帳日期'].to_list()
        data_bank_invoive_date = df_modify['發票日期'].to_list()
        data_bank_invoice_num = df_modify['發票號碼'].to_list()
        data_bank_credit_mount = df_modify['信用卡'].to_list()
        data_bank_bill_mount = df_modify['票據'].to_list()
        data_bank_cash_mount = df_modify['現金'].to_list()
        data_bank_insurance_mount = df_modify['保險'].to_list()
        data_bank_other_mount = df_modify['其他'].to_list()
        data_bank_class_type = df_modify['類別'].to_list()
        data_bank_class_mount = df_modify['金額'].to_list()

        # print(data_bank_dep_info)




        # =======資料取出完成, 類型為list===
        # print(data_bank_class_type)
    
        # =======做類別判斷======
        index_list_internal = []
        index_list_prepay = []
        index_list_transfer = []
        for j in range(len(data_bank_class_type)):
            if data_bank_class_type[j] == '內轉':
                index_list_internal.append(j)
            elif data_bank_class_type[j] == '預收訂金':
                index_list_prepay.append(j)
            elif data_bank_class_type[j] == '電匯':
                index_list_transfer.append(j)
            else:
                pass
        list_internal_mount = [data_bank_class_mount[i]
                               for i in index_list_internal]
        list_prepay_mount = [data_bank_class_mount[i] for i in index_list_prepay]
        list_transfer_mount = [data_bank_class_mount[i]
                               for i in index_list_transfer]
        # ======類別判斷區塊結束======
    
        # ======部門＆工單號碼串接======
    
        data_bank_dep_info_code = [i[0:2] for i in data_bank_dep_info]
        data_c_dep_wnum = list(zip(data_bank_work_num, data_bank_dep_info_code))
    
        data_wnum_with_depcode = [
            f'{data_c_dep_wnum[i][0]}-{data_c_dep_wnum[i][1]}' for i in range(len(data_c_dep_wnum))]
    
        work_num_content = '\n'.join(data_wnum_with_depcode)
    
        # ======打印資料======
        print(f'---------{area_mode}{date_list_unique[i]}----------')
        print(f'{date_list_unique[i]}信用卡總: {sum(data_bank_credit_mount)}')
        print(f'{date_list_unique[i]}票據總額: {sum(data_bank_bill_mount)}')
        print(f'{date_list_unique[i]}現金總額: {sum(data_bank_cash_mount)}')
        print(f'{date_list_unique[i]}保險總額: {sum(data_bank_insurance_mount)}')
        print(f'{date_list_unique[i]}其他總額: {sum(data_bank_other_mount)}')
        print(f'{date_list_unique[i]}內轉總額: {sum(list_internal_mount)}')
        print(f'{date_list_unique[i]}預收總額: {sum(list_prepay_mount)}')
        print(f'{date_list_unique[i]}電匯總額: {sum(list_transfer_mount)}')
        print(f'{date_list_unique[i]}工單部門: \n{work_num_content}')
        print(f'---------{area_mode}{date_list_unique[i]}----------')
        # ======打印資料區塊======

        # date_1 = date_list_unique[i][-2]
        # print(date_1)
        result = {'日期': int(date_list_unique[i][-2:]) , '刷卡（含稅）': sum(data_bank_credit_mount),
            '現金/電匯\n（含稅）': sum(data_bank_cash_mount) + sum(list_transfer_mount) + sum(data_bank_bill_mount) + sum(list_prepay_mount),
            '應收帳款\n（含稅）': sum(data_bank_insurance_mount) + sum(data_bank_other_mount),
            '內轉（含稅）': sum(list_internal_mount)}

        # print(result)
        results.append(result)
    return results



def select_report():
    now_path = os.getcwd().replace('\\', '/')
    file_list = os.listdir(now_path)
    if 'templates' in file_list:
        templates_path = f'{now_path}/templates'
        # report_lists = os.listdir(templates_path)
        # print(report_lists)
        print('報表路徑 : ' + templates_path)
        return templates_path
    else:
        pass

    

    # print(file_list)
    # path = filename
    # wb = load_workbook(path)
    # sheet = wb['11002']

    # column_1st = [i.value for i in list(sheet.columns)[0]]  # 第一column
    # row_name = [i.value for i in list(sheet.rows)[4]]

    # column_name = ['刷卡（含稅）', '現金/電匯\n（含稅）', '應收帳款\n（含稅）', '內轉（含稅）']

    # for i in column_name:
    #     sheet.cell(row=column_1st.index(int(date))+1,
    #                column=row_name.index(i)+1, value=result[i])


# path_1 = 'D:\\000_git_repo_projects\\Autoreportplus_v13\\databackup\\202109\\2021090120210901_st0904084551.xlsx'
# aa = '濱江'
# process_data(aa, path_1)
# process_data(path_1)
# user_input = input('請輸入廠別  1. 濱江廠  2. 士林廠    >>    ')
# if user_input == '1':
#     aa = '濱江'
#     process_data(aa, path_1)
# else:
#     aa = '士林'
#     process_data(aa, path_1)
# select_report()
