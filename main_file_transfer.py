import re
import os
from openpyxl import load_workbook
from datetime import datetime


# 判斷資料夾是否存在

def check_dir(name):
    dir_path = f'./{name}'
    if os.path.isdir(dir_path) == True:
        print(f'程式狀態 : {name} 資料夾已經存在.') 
    else:
        os.mkdir(dir_path)
        print(f'程式狀態 : {name} 已完成建立.')

#dir_exist('databackup')


# 讀取檔案

def transfer_file(filename):
    check_dir('databackup')
    wb = load_workbook(filename) # 開啟excel檔
    sheet = wb.worksheets[0] # 讀取唯一分頁

    # 定位資料表
    column_list = [i.value for i in list(sheet.columns)[0]]
    # print(column_list)

    #read_file('2101_fULL.xlsm')
    start_index = column_list.index('經銷商代號')
    # print(start_index)
    # sheet.delete_rows(1, start_index) # 加工excel
    # 取出系統轉出報表日期區間
    # print(start_index)

    # 取出做備份檔案名稱
    for i in column_list:
        if '期間' in i:
            file_date_index = column_list.index(i)
            file_date_name = re.sub(u'\D', '', i)
        else:
            pass
    date_ym_start = file_date_name[0:6] #202101
    date_ym_end = file_date_name[8:14]  #202101
    # print(date_ym_start, date_ym_end)
    
    local_path = os.getcwd()

    # 判斷系統轉出資料報表range並放置對應資料夾
    if date_ym_start == date_ym_end:
        if os.path.isdir(f'{local_path}\\databackup\\{date_ym_start}') == True:
            print(f'程式狀態 : {date_ym_start} 資料夾已經存在.')
            pass
        else:
            os.mkdir(f'{local_path}\\databackup\\{date_ym_start}')
            print(f'程式狀態 : 建立 {date_ym_start} 資料夾.')
    
    else:
        if os.path.isdir(f'{local_path}\\databackup\\Peroid_區間報表備份') == True:
            print('程式狀態 : Peroid_區間報表備份 資料夾已存在')
            pass
        else:
            os.mkdir(f'{local_path}\\databackup_dir\\Peroid_區間報表備份')
            print('程式狀態 : Peroid_區間報表 資料夾已建立')

    # EXCEL加工, 移除不必要的rows
    sheet.delete_rows(1, start_index) # 加工excel
    print('程式狀態 : EXCEL 加工完畢, 開始存檔')
    
    # 把加工過後的EXCEL存放到對應位置, 檔名後面串接時間
    sys_time = datetime.now().strftime('%m%d%I%M%S')
    save_filename = f'{file_date_name}_st{sys_time}'
    if date_ym_start == date_ym_end:
        wb.save(f'{local_path}\\databackup\\{date_ym_start}\\{save_filename}.xlsx')
        wb.close()
        save_file_path = f'{local_path}\\databackup\\{date_ym_start}\\{save_filename}.xlsx'
        return save_file_path
    else:
        wb.save(f'{local_path}\\databackup\\Peroid_區間報表備份\\{save_filename}.xlsx')
        save_file_pat = f'{local_path}\\databackup\\Peroid_區間報表備份\\{save_filename}.xlsx'
        wb.close()
        return save_file_path
    print('程式狀態 : 資料存檔完畢')
    print(f'檔案名稱 : {save_filename}.xlsx')
    

# 檔案執行區
# databackup_dir('databackup')
# read_file('2101_fULL.xlsm')








