from watchdog.observers import Observer
from watchdog.events import *
import time
import os
import main_file_transfer 
from main_file_process import *
from getpass import getpass
import pandas as pd


class FileEventHandler(FileSystemEventHandler):
    def __init__(self):
        FileSystemEventHandler.__init__(self)
        # self.report_area = report_area

    def on_created(self, event):
        if event.is_directory:
            print("directory created:{}".format(event.src_path))
        else:
            # print("file created:{}".format(event.src_path))
            #開始進行檔案名稱判定
            file_path = os.getcwd()
            # file_path_list = os.listdir(file_path)
            # print(file_path_list)
            # print(event.src_path)
            # print(filePath)
            file_name = event.src_path.replace(f'{filePath}', '')
            print(file_name)
            if file_name == '200_1.xlsx':
                time.sleep(1)
                a = main_file_transfer.transfer_file(event.src_path)
                process_data(a)
                os.remove(event.src_path)
                time.sleep(1)
                print(f'程式狀態 : 刪除檔案 {file_name}')
                # observer.stop()
                # observer.join()
            elif file_name == '200_1.xlsm':
                time.sleep(1)
                a = main_file_transfer.transfer_file(event.src_path)
                process_data(a)
                time.sleep(1)
                os.remove(event.src_path)
                print(f'程式狀態 : 刪除檔案 {file_name}')
            elif file_name == '200_1.xls':
                df = pd.read_excel(f'{filePath}/200_1.xls', engine = 'xlrd')
                df.to_excel(f'{filePath}/200_1.xlsx', engine = 'openpyxl' ,index = None)
                time.sleep(1)
                os.remove(f'{filePath}/200_1.xls')
                print(f'程式狀態 : 刪除檔案 {file_name}')
            else:
                print(f'程式狀態 : 檔名錯誤, 請重新確認!!!')



if __name__ == "__main__":
    user_name = input('請輸入使用者名稱 : ')
    user_ppwd = getpass('請輸入使用者密碼 : ')
    if user_name == 'vero':
        if user_ppwd == '0521':
            local_path = os.getcwd().replace('\\', '/')
            observer = Observer()
            event_handler = FileEventHandler()
            local_path = os.getcwd()
            dir_name_entrance = '報表檔案接收入口'
            dir_name_backup = 'databackup'
            dir_name_output = 'output'
            dir_name_templates = 'templates'
            main_file_transfer.check_dir(dir_name_entrance)
            main_file_transfer.check_dir(dir_name_backup)
            main_file_transfer.check_dir(dir_name_output)
            main_file_transfer.check_dir(dir_name_templates)
            filePath = f'{local_path}/{dir_name_entrance}/'
            os.system(f'start {filePath}')
            print('監控路徑 : ' + filePath)
            observer.schedule(event_handler,filePath,True)
            observer.start()
            try:
                while True:
                    time.sleep(1)
            except KeyboardInterrupt:
                observer.stop()
                observer.join()
